import * as BABYLON from 'babylonjs';

function createScene(engine: BABYLON.Engine): BABYLON.Scene {
    const scene: BABYLON.Scene = new BABYLON.Scene(engine);
    const camera: BABYLON.FreeCamera = new BABYLON.FreeCamera('camera1',
        new BABYLON.Vector3(0, 0, 0), scene);

    camera.setTarget(BABYLON.Vector3.Zero());
    const light: BABYLON.HemisphericLight = new BABYLON.HemisphericLight('light1',
        new BABYLON.Vector3(0, 1, 0), scene);

    return scene;
}

function createSphere(scene: BABYLON.Scene): BABYLON.Mesh {
    const sphere: BABYLON.Mesh = BABYLON.Mesh.CreateSphere('sphere1', 16, 10,
        scene, true, BABYLON.Mesh.BACKSIDE);

    sphere.position.set(0, 0, 0);
    const material: BABYLON.StandardMaterial = new BABYLON.StandardMaterial("Mat1", scene);
    material.diffuseTexture = new BABYLON.Texture("assets/4.png", scene);
    sphere.material = material;
    return sphere;
}

function init(): void {
    const canvas: HTMLCanvasElement = document.getElementById("my-canvas") as HTMLCanvasElement;
    const engine: BABYLON.Engine = new BABYLON.Engine(canvas, true, {
        preserveDrawingBuffer: true,
        stencil: true
    });

    const scene: BABYLON.Scene = createScene(engine);

    const sphere: BABYLON.Mesh = createSphere(scene);


    engine.runRenderLoop(() => {
        scene.render();
        sphere.rotation.x += 0.01;
        sphere.rotation.y += 0.005;
        (window as any).meter.tick();
    });

    window.addEventListener("resize", () => {
        engine.resize();
    });
}

window.addEventListener("load", () => {
    init();
});
